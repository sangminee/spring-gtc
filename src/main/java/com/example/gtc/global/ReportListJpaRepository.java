package com.example.gtc.global;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ReportListJpaRepository extends JpaRepository<ReportList,Long> {
}
