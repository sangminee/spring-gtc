# 그릿지 테스트 🍀

챌린지 기간 : 2022. 07. 25 ~ 2022. 08. 07 (2주)

## E-R Diagram

https://www.erdcloud.com/d/2xA9vhtHLcpoqMCcC
![erd 그릿지](https://user-images.githubusercontent.com/81500474/184348347-192762fa-4d33-48ec-a442-6b6bbe8a728d.png)


## 폴더 구조  

![image](https://user-images.githubusercontent.com/81500474/183300197-039f8bd7-c4db-462b-a189-7eb89177e2fa.png)

## 사용 기술

- Spring boot <br>
- Spring Data Jpa <br>
- Swagger <br>
- AWS RDS <br>
- AWS EC2 <br>


## Commit & Convention 적용 
https://minee.tistory.com/47

## 이슈 해결
1. user은 이미 MySQL에 예약어로 존재 <br>
org.hibernate.tool.schema.spi.commandacceptanceexception: error executing ddl "create user...... 
